FROM apache/superset:latest-dev
USER root
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -qq libldap2-dev libsasl2-dev
COPY requirements-nina.txt .
RUN pip3 install --no-cache -r requirements-nina.txt
USER superset
